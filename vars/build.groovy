#!/usr/bin/env groovy
import static ru.org.online.pipeline.Utilities.*

def getParams(body, String key) { getUserEnvironment(body)."${key}" }

def call(body) {
    pipeline {
        agent any
        environment {
            CC = 'clang'
            ENVIRONMENT = getParams body, 'ENVIRONMENT'
            SUCCESS_MAILTO = getParams body, 'SUCCESS_MAILTO'
            FAILURE_MAILTO = getParams body, 'FAILURE_MAILTO'
        }
        // parameters {
        //     string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')
        // }
        stages {
            // stage ('Prepare') {
            //     steps {
            //
            //     }
            // }
            stage('Build') {
                steps {
                    sh 'npm --version'
                    sh "ls -la"
                    echo env
                    echo '>>>>>>>>>>>>>><<<<<<<<<'
                }
            }
            stage('Browser Tests') {
                parallel {
                    stage('Chrome') {
                        steps {
                            echo "Chrome Tests"
                        }
                    }
                    stage('Firefox') {
                        steps {
                            echo "Firefox Tests"
                        }
                    }
                }
            }
            stage('Deploy') {
                steps {
                    input message: 'User input required', ok: 'Release!',
                            parameters: [choice(name: 'RELEASE_SCOPE', choices: 'patch\nminor\nmajor', description: 'What is the release scope?')]

                    // script {
                    //     env.RELEASE_SCOPE =
                    // }
                    // echo "${env.RELEASE_SCOPE}"

                }
            }

        }
        post {
            failure {
                echo 'failure!!! =((('
            }
            success {
                echo 'success!!! =)))'
            }
            always {
                echo 'Clean'
            }
            unstable {
                echo 'I am unstable :/'
            }
        }
    }
}
