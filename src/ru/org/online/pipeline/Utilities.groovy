#!/usr/bin/groovy
package ru.org.online.pipeline

class Utilities {
    static def npm(script) {
        script.sh "npm --version"
    }

    static def getUserEnvironment(body) {
        def params = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = params
        body()

        return params
    }
}
